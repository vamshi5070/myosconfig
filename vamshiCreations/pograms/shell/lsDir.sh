#!/usr/bin/env bash
set -euo pipefail

dir=$(pwd)

echo $dir
arr=(okular emacs)
file=$dir/xmenu.sh

printf "#!/bin/sh\nxmenu <<EOF | sh &\nalacritty\talacritty\n">$file

chmod a+x $file

echo ${arr[0]}

elements=$(ls $dir)

# cd /home/vamshi

for i in $elements
do

    if [ -d $i ]
    then
        cd $i
        /home/vamshi/vamshiCreations/pograms/shell/lsDir.sh


        printf "$i\t$dir/$i/xmenu.sh\n" >> $file
    else
        if [[ $i == "*.pdf"  ]]
        then
            printf "$i\t${arr[0]} $dir/$i\n" >> $file
        else
            if [[ $i == "xmenu.sh" ]]
            then
                continue
            else
            printf "$i\t${arr[1]} $dir/$i\n" >> $file
            fi
        fi
    fi
done

echo "EOF">>$file
