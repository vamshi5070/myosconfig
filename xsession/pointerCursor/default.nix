{ config, lib, pkgs, ... }:
{
  home-manager.users.vamshi.xsession.pointerCursor = {
    name = "vanilla-dmz";
    package = pkgs.vanilla-dmz;
    size = 64;
  };
}
