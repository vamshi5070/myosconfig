{ config, lib, pkgs, ... }:{
  home-manager.users.vamshi.xresources = {
    extraConfig = "#define KSYMRIGHT XK_h
                   #define KSYMLEFT XK_l
                   #define KSYDOWN  XK_j
                   #define KSYMUP   XK_k";
    properties = {
      "xmenu.borderWidth"="0";
      "xmenu.separatorWidth"="39";
      "xmenu.height"="49";
      "xmenu.width"="80";
      # "xmenu.gap"="2";
      "xmenu.background"="#0f0f0f";
      "xmenu.foreground"="#bbbbbb";
      "xmenu.selbackground"="#bbbbbb";
      "xmenu.selforeground"="#0f0f0f";
      "xmenu.separator"="#555753";
      "xmenu.triangle_width"="2";
      "xmenu.triangle_height"="4";
      "xmenu.font" = "Lucida MAC:size=18";
      # "xmenu.alignment"="right";
    };
  };
}
