{ config, lib, pkgs, ... }:

{

   environment.systemPackages = with pkgs; [
     vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
     wget
     pavucontrol
     libreoffice
     sxiv
     unzip
     ripgrep
     fd
     transmission-gtk
     xfce.xfce4-terminal
     okular
     ghc
     pcmanfm
     xmenu
     kupfer
     libnotify
    # (all-hies.selection { selector = p: { inherit (p) ghc8104; }; })
     xmobar
     dmenu
     atom
     gnome.gnome-disk-utility
     brightnessctl
     openssh
     kdenlive
     scrot
     xorg.xrdb
     perl
     vlc
     gcc
     audacious
     spotify
     tree
     brave
   ];
}
