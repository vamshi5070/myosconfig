{
  description = "NixOS configuration";

  inputs = {
    home-manager = {
      url = "github:rycee/home-manager/release-21.05";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    nur.url = "github:nix-community/NUR";
    #emacs-ng.url = "github:emacs-ng/emacs-ng";
    emacs-overlay.url = "github:nix-community/emacs-overlay";
    nix-doom-emacs.url = "github:vlaci/nix-doom-emacs";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-21.05";
    unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    master.url = "github:nixos/nixpkgs/master";
  };

  outputs = inputs@{ self,home-manager,nur,nix-doom-emacs,nixpkgs, ... }:
let
      inherit (builtins) listToAttrs attrValues attrNames readDir;
      inherit (nixpkgs) lib;
      inherit (lib) removeSuffix;

      pkgs = (import nixpkgs) {
        system = "x86_64-linux";
        config = {
          allowUnfree = true;
          retroarch = {
            enableCitra = true;
            enableParallelN64 = true;
            enableDesmume = true;
            enableSnes9x = true;
            enableNestopia = true;
            enableVbaNext = true;
          };
        };
        overlays = attrValues self.overlays;
      };
in
{
overlays =
        let
          overlayFiles = listToAttrs (map
            (name: {
              name = removeSuffix ".nix" name;
              value = import (./overlays + "/${name}");
            })
            (attrNames (readDir ./overlays)));
        in
          overlayFiles // {
            nur = final: prev: {
              nur = import inputs.nur { nurpkgs = final.unstable; pkgs = final.unstable; };
            };
            emacs-overlay = inputs.emacs-overlay.overlay;
       unstable = final: prev: {
              unstable = import inputs.unstable {
                system = final.system;
                config = {
                  allowUnfree = true;
                  retroarch = {
                    enableCitra = true;
                    enableParallelN64 = true;
                    enableDesmume = true;
                    enableSnes9x = true;
                    enableNestopia = true;
                    enableVbaNext = true;
                  };
                };
              };
            };
            master = final: prev: {
              master = import inputs.master {
                system = final.system;
                config = {
                  allowUnfree = true;
                  retroarch = {
                    enableCitra = true;
                    enableParallelN64 = true;
                    enableDesmume = true;
                    enableSnes9x = true;
                    enableNestopia = true;
                    enableVbaNext = true;
                  };
                };
              };
            };
          };


    nixosConfigurations = {
     cosmos = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./configuration.nix
          ./pkgs
	        ./services/redshift
	        ./services/starship
	        ./services/randomBg
                #./services/emacs
	        ./services/dunst
	        # ./services/unclutter
	        # ./services/xcape
	        ./services/playerctld
	        ./services/cbatticon
	        ./programs/alacritty
	        ./programs/git
	        #./progras/emacs
	        # ./programs/vscode
	        ./programs/ssh
	        ./programs/fish
	        ./programs/bash
	        ./programs/tmux
	        ./programs/ncmpcpp
	        ./programs/packages
	        #./programs/gnome-terminal
	        ./programs/firefox
	        ./programs/exa
	        ./programs/htop
	        ./programs/neovim
          ./xsession/xresources
          ./xsession/pointerCursor
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
	          home-manager.users.vamshi = { pkgs, ... }: {
               imports = [ nix-doom-emacs.hmModule ];
               programs.doom-emacs = {
                 enable = true;
                 doomPrivateDir = ./programs/emacs/doom.d;
                 emacsPackagesOverlay = self: super: {
                   magit-delta = super.magit-delta.overrideAttrs (esuper: {
                     buildInputs = esuper.buildInputs ++ [ pkgs.git ];
                   });
                  };
                };
              };
          }
        ];
        inherit pkgs;
    };
    };
};
}

