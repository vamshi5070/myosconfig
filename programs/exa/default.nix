{ pkgs, config, lib, inputs, ... }:{
   home-manager.users.vamshi.programs.exa = {
      enable = true;
      enableAliases = true;
   };
}
