{ pkgs, config, lib, inputs, ... }:{
        home-manager.users.vamshi.programs.fzf = {
	enable = true;
	enableBashIntegration = true;
	enableFishIntegration = true;
	};
}	
