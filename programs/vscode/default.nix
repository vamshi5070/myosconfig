{ inputs, config, lib, pkgs, ... }:{
  	home-manager.users.vamshi.programs.vscode = {
      enable = true;
      package = pkgs.vscodium;
      haskell = {
        enable = true;
        # hie =
        #   { enable = true;
        #     executablePath = (import "/home/vamshi/src/haskell-ide-engine" {}).hies + "/bin/hie-wrapper";
        #   };
      };

    };
}
