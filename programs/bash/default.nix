{ pkgs, config, lib, inputs, ... }:{
  	home-manager.users.vamshi.programs.bash = {
	enable = true;
	historyIgnore = ["ls" "cd" "exit"];
	bashrcExtra = "eval \"$(starship init bash)\"
                  shopt -s autocd
                  set -o vi";
  shellAliases = {
                   up = "sudo nixos-rebuild switch";
                   e = "exit";
                   c = "clear";
                   # ls = "exa";
                   # ll = "exa -la";
  };
	};
}
