{pkgs,config,lib, inputs, ... }:{
        home-manager.users.vamshi.programs.neovim = {
	   enable =  true;
 	plugins = with pkgs.vimPlugins; [
  		#yankring
  		vim-nix
 	 	{ plugin = vim-startify;
    		config = "let g:startify_change_to_vcs_root = 0";
 	       }
                fzf-vim
		haskell-vim
		gruvbox
	];	
        extraConfig = ''
                        set statusline=emacs
			colorscheme gruvbox
			set background=dark
                      '';
         vimAlias = true;
         viAlias = true;
   	};
}
