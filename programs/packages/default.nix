{ inputs,config, lib, pkgs, ... }:

let
    haskell-env = pkgs.unstable.haskellPackages.ghcWithHoogle (
  hp: with hp; [
    xmonad
    xmonad-contrib
    xmonad-extras
    apply-refact
    haskell-language-server
    brittany
    cabal-install
    hlint
    xmobar
    stack
    # filepath_1_4_2_1
    # directory_1_3_6_2
  ]
);

in
{
  home-manager.users.vamshi.home.packages = with pkgs; [
    haskell-env
  ];
}
