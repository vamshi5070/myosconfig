# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{inputs,lib, config, pkgs, ... }:
#let 
#   overrides = self: super: rec {
#  haskell-mode = self.melpaPackages.haskell-mode;
#};
#in

# let

  # all-hies = import (fetchTarball "https://github.com/infinisil/all-hies/tarball/master") {};

# in
  
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "cosmos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.wireless = {
               enable = true;  # Enables wireless support via wpa_supplicant.
	             networks.Varikuti.pskRaw = "2f5385967f945b489e113af64fb060b86319cf195898249a8cd0189e63078cbd";
        # };
        interfaces = ["wlp2s0"];
       userControlled.enable = true;
  };
  # Set your time zone.
  time.timeZone = "Asia/Kolkata";

 # environment.shellInit = ''
 #    xsetroot -solid "#1c2023"
 #    xsetroot -cursor_name left_ptr
 #  '';


  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp1s0.useDHCP = true;
  networking.interfaces.wlp2s0.useDHCP = true;

  hardware.bluetooth = {
       enable = true;
       powerOnBoot = true;
     };
 nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
   };

 fonts = {
   fonts = with pkgs; [
    pkgs.google-fonts
    # pkgs.input-fonts
    #     pkgs.vistafonts
    #         pkgs.font-awesome
    #             pkgs.hack-font
    #                 pkgs.monoid
    #                 		 pkgs.powerline-fonts
    #                 		     (nerdfonts.override { fonts = [  "VictorMono" "SourceCodePro" "Mononoki" ]; })
    #                 		     		pkgs.ubuntu_font_family
    #                 		     				pkgs.emacs-all-the-icons-fonts
    #                 		     				    pkgs.iosevka
    #
    #                 		     				      ];
 (nerdfonts.override { fonts = [ "Hasklig" "Inconsolata" "VictorMono" "SourceCodePro" "Mononoki" ]; })
		pkgs.ubuntu_font_family
		pkgs.emacs-all-the-icons-fonts
   pkgs.spleen
  # unstable.cozette
    # pkgs.apple-color-emoji
    # pkgs.noto-fonts-cjk
     ];
   fontconfig = {
     enable = true;
  defaultFonts = {
        emoji = [ "Apple Color Emoji" ];
        monospace = [ "Spleen" ];
      };
   };
 };
  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  # };

  # Enable the X11 windowing system.
  #services.xserver.enable = true;


  # Enable the GNOME 3 Desktop Environment.
  #services.xserver.displayManager.gdm.enable = true;
  #services.xserver.desktopManager.gnome.enable = true;
  
  services.xserver =
    {
      enable = true;
      # Configure keymap in X11
      layout = "us";
      xkbOptions = "caps:swapescape";
      libinput= {
        enable = true;
	touchpad = {
        naturalScrolling = true;      # Enable touchpad support (enabled default in most desktopManager).
		    additionalOptions = ''
                                                                      Option "AccelSpeed" "1.0"        # Mouse sensivity
                                                                      Option "TapButton2" "0"          # Disable two finger tap
                                                                      Option "VertScroll Delta" "-180"  # scroll sensitivity
                                                                      Option "HorizScroll Delta" "-180"
                                                                      Option "FingerLow" "40"          # when finger pressure drops below this value, the driver counts it as a release.
                                                                      Option "FingerHigh" "70"
                                                                      '';
		};
		  };
       windowManager.xmonad = {
                               enable = true;
		               enableContribAndExtras = true;
		        };

                         displayManager = {
			  defaultSession = "none+xmonad";
		       	autoLogin.enable = true;
           		autoLogin.user = "vamshi";
			   };
                           # desktopManager.wallpaper.mode = "fill"; # from ~/.background-image
  };
  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  #hardware.pulseaudio.enable = true;
  hardware.pulseaudio = {
               enable = true;
               extraModules = [ pkgs.pulseaudio-modules-bt ];
               package = pkgs.pulseaudioFull;
               support32Bit = true; # Steam
               # extraConfig = ''7
               #                # load-module module-bluetooth-policy auto_switch=2
               #                	             # load-module module-switch-on-connect
               #                	             	             # load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1
               #                	             	                            #   '';
               #                	             	                              };

    };
  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;
  # nixpkgs.overlays = [
  #        (self: super: { hie-nix = import "~/src/hie-nix" {}; })
  #      ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.vamshi = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    #shell = pkgs.fish;
  };
#((emacsPackagesFor emacs).overrideScope' overrides).emacs.pkgs.withPackages
#  (p: with p; [
    # here both these package will use haskell-mode of our own choice
 #   ghc-mod
 #   dante
 # ])
  # List packages installed in system profile. To search, run:
  # $ nix search wget
# nixpkgs.config.allowBroken = true;
  services.gvfs.enable = true;
  services.udisks2.enable = true;
  services.devmon.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
      # Security and networking
  security.sudo.wheelNeedsPassword = false;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

}

