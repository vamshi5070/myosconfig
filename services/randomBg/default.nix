{ pkgs, config, lib, inputs, ... }:{
  # home.username = "vamshi";
  # home.homeDirectory = "/home/vamshi";
  home-manager.users.vamshi.services.random-background = {
  	enable = true;
		imageDirectory = "/home/vamshi/pictures";
		interval = "34m";
  };
}
