
{ pkgs,  ... }: {
  home-manager.users.vamshi = {
    programs.emacs = {
      enable = true;
      package = pkgs.emacs;
      extraPackages = epkgs:
      with epkgs; [
      #               vterm
      #               dante
      #               pdf-tools
                     evil
	             evil-collection
		  #   nix-mode
		     which-key
                     highlight-numbers
       		    general
		  #   kaolin-themes
      #               vscode-dark-plus-theme
                     haskell-mode
      #               sudo-edit
                     use-package
                     org-bullets
			#               doom-themes
      #               doom-modeline
      #               writeroom-mode
      #               perspective
      #               #auto-sudoedit

      # all-the-icons-dired
	    # all-the-icons
      # rainbow-delimiters
      # # zen
      # writeroom-mode
      # mixed-pitch
      # # ivy
      # ivy
      # ivy-rich
      # all-the-icons-ivy-rich
      # all-the-icons-ivy
      # # counsel
	    # counsel
      # counsel-projectile
	    # swiper
	    # # projectile
	    # projectile
      #               centaur-tabs
      #               solaire-mode
      #               smex

        ];
    };

    # home.packages = [
    #   (pkgs.makeDesktopItem {
    #     terminal = "false";
    #     type = "Application";
    #     name = "emacsclient";
    #     genericName = "Text editor";
    #     desktopName = "Emacs client";
    #     mimeType = "text/plain";
    #     exec = "emacsclient -s /tmp/emacs1000/server -c %F";
    #     categories = "Development;TextEditor;Utility;";
    #     icon = "emacs";
    #   })
    #   pkgs.clang
    # ];

    # services.emacs.enable = true;
  services.emacs = {
    enable = true;
    client = {
      enable = true;
      arguments = [ "-c" ];
    };
    socketActivation.enable = false;
  };
    # systemd.user.services.emacs.Service.Environment =
    #   "PATH=/run/current-system/sw/bin:/etc/profiles/per-user/vamshi/bin";

  #   home.file.".emacs.d/init.el".source = ./init.el;
  #   home.activation.emacs = {
  #     before = [ ];
  #     after = [ ];
  #     data = "$DRY_RUN_CMD mkdir -p ~/.emacs.d/autosave";
  #   };
  };
}

