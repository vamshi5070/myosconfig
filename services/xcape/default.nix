{ config, lib, pkgs, ... }:{
    home-manager.users.vamshi.services.xcape = {
  	enable = true;
    mapExpression = { Hyper_L ="Tab"; Hyper_R = "backslash";};
    };
}
