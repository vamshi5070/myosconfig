{ pkgs, config, lib, inputs, ... }:{
  #redshift
location = {
            latitude = 17.492409;
            longitude = 78.281284;
};
services.redshift = {
     enable = true;
     #latitude = "17.492409";
     #longitude = "78.281284";
     #tray = true;
		temperature = {
		  day = 4000;
		  night = 4000;
		};
    extraOptions = [
        "-g" "0.8:0.8:0.8"
    ];
		brightness  = {
    	                day = "0.40";
			night = "0.40";
    };
	};
}
